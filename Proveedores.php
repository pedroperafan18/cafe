<?php
// Configura la información de tu cuenta
include("conexion.php");
session_start();
if (isset($_SESSION['s_username'])) {
echo "Bienvenido, has ingresado como <strong>".$_SESSION['s_username']."</strong>";
}
else
{
echo "Secion inexistente";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Cafeteria D´Coffe</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="menu">
		<ul>
			<?php if(!(isset($_SESSION['s_username'])))
							{ ?>  
                            	<!-- No esta Logeado -->
							 	<li><a href="login.php">Iniciar Sesion</a></li>
                            <?php 
							} 
							else 
							{ ?>  
                            	<!-- Ya esta Logeado -->
                            	
                                 <?php 	if ($_SESSION['s_username'] == "EdithMM")
										{?>
                                            <li class="end"><a href="logout.php">Cerrar Sesion</a></li>
                                            <li class="end"><a href="Empleados.php">Empleados</a></li>
                                			<li class="end"><a href="Productos.php">Productos</a></li>
                            			    <li class="end"><a href="Proveedores.php">Proveedores</a></li>
								<?php	}
										else
										{
								?>
							 				<li class="end"><a href="logout.php">Cerrar Sesion</a></li>
							 				<li class="end"><a href="Ventas.php">Mis Ventas</a></li>
							 				<li class="end"><a href="puntoventa.php">Punto de venta</a></li>
								
								<?php	}
								?>
        					<?php 
							} ?>                                 
		</ul>
	</div>
	<!-- end #menu -->
	<div id="header">
		<div id="logo">
			<h1><a href="inicio.php">D´Coffe</a></h1>
		</div>
		<div id="search">
			<form method="get" action="">
				<fieldset>
					<input type="text" name="s" id="search-text" size="15" value="enter keywords here..." />
					<input type="submit" id="search-submit" value="GO" />
				</fieldset>
			</form>
		</div>
	</div>
	<div id="splash">&nbsp;</div>
	<!-- end #header -->
	<div id="page">
		<div id="page-bgtop">
			<div id="page-bgbtm">
				<div id="content">
					<div class="post">
							 	<li><a href="AgregarProveedor.php"><img src="images/agregarproveedor.jpg" width="600" height="200"></a></li>
							 	<li><a href="ModificarProveedor.php"><img src="images/modificarproveedor.jpg" width="600" height="200"></a></li>
							 	<li><a href="Eliminarproveedor.php"><img src="images/eliminarproveedor.jpg" width="600" height="200"></a></li>
							 	<li><a href="ConsultarProveedor.php"><img src="images/consultarproveedor.jpg" width="600" height="200"></a></li>
					</div>
					<div class="post">
						<div class="entry">
						</div>
					</div>
					<div style="clear: both;">&nbsp;</div>
				</div>
				<!-- end #content -->
				<div id="sidebar">
					<ul>
						<li>
							<h2>Opciones</h2>
							<ul>
								<?php if(!(isset($_SESSION['s_username'])))
							{ ?>  
                            	<!-- No esta Logeado -->
							 	<li><a href="login.php">Iniciar Sesion</a></li>
                            <?php 
							} 
							else 
							{ ?>  
                            	<!-- Ya esta Logeado -->
                            	
                                 <?php 	if ($_SESSION['s_username'] == "EdithMM")
										{?>
                                            <li><a href="Empleados.php">Empleados</a></li>
											<li><a href="Productos.php">Productos</a></li>
											<li><a href="Proveedores.php">Proveedores</a></li>
								<?php	}
										else
										{
								?>
							 				<li><a href="puntoventa.php">Punto de venta</a></li>
											<li><a href="Ventas.php">Mis ventas</a></li>
								
								<?php	}
								?>
        					<?php 
							} ?>      
							</ul>
						</li>
					</ul>
				</div>
				<!-- end #sidebar -->
				<div style="clear: both;">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- end #page -->
</div>
<div id="footer-wrapper">
	<div id="three-columns">
		<div id="column1">
			<h2>Terminos y condiciones</h2>
			<ul>
				<li><a href="https://mega.nz/#!at5XwShZ!_N47n57Bs4jEtrRc_3PqtDTl-h90VT3QzTOrfMh7uRQ" target="_blank">Descargar documento</a></li>
			</ul>
		</div>
		<div id="column2">
			<h2>Reglamento</h2>
			<ul>
				<li><a href="https://mega.nz/#!m54USR5Z!y1uly8pUSFpUJuwD1V5_isaU6rlc3BckSXjf2knS0fc" target="_blank">Leer reglamento</a></li>
				<li><a href="#">Acerca de la tienda</a></li>
			</ul>
		</div>
		<div id="column3">
			<h2>Redes Sociales</h2>
			<ul>
				<li><a href="https://www.facebook.com/Cafeteriadcoffe/?skip_nax_wizard=true">Facebook</a></li>
				<li><a href="#">Twitter</a></li>
				<li><a href="#">Youtube</a></li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<p>D´Coffe Cafeteria, por que necesitas un tiempo para disfrutar las cosas dulces</p>
	</div>
	<!-- end #footer -->
</div>
</body>
</html>
